module.exports = {
  transpileDependencies: ["vuetify"],
  devServer: {
    compress: true,
    hot: true,
    open: true,
    port: 9014
  }
};
